// Create a POST request

options = {
    method: 'POST',
    body: JSON.stringify({
        title: 'new bag',
        price: 18,
        description: 'A new product in the store',
        image: 'https://i.pravatar.cc',
        category: 'bags',
    })
}

fetch('https://fakestoreapi.com/products', options)
  .then((res) => res.json())
    .then((json) => console.log(json))