options = {
  method: 'PUT',
  body: JSON.stringify({
    title: 'updated product',
    price: 18,
    description: 'The product is unpdated',
    image: 'https://i.pravatar.cc',
    category: 'bag',
  }),
};

fetch('https://fakestoreapi.com/products/7', options)
  .then((res) => res.json())
  .then((json) => console.log(json));
