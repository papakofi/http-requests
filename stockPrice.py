from bs4 import BeautifulSoup
import requests

def get_price():
    """
    It gets the price of a stock from Yahoo Finance
    @return: The price of the stock
    """
    req = requests.get("https://finance.yahoo.com/quote/GC%3DF?p=GC%3DF")
    soup = BeautifulSoup(req.text, "html.parser")
    price = soup.find('div', {'class': 'D(ib) Mend(20px)'})
    return price[0]

while True:
    print("The current price of Gold: "+ str(get_price()) )