from bs4 import BeautifulSoup
import requests


# Get the HTML from the page
datasus_url = "https://www.datasustl.com/"
req = requests.get(datasus_url)

soup = BeautifulSoup(req.text, "html.parser")
print(soup.title.string)

# find the first occurrence of a tag
print(soup.find("h4"))


# Pretty print the HTML
print(soup.prettify())
